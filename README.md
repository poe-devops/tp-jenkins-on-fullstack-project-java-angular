# Application `formation` - gestion d'un centre de formation

## Description de l'application

L'application devra permettre aux gestionnaires du centre de formation de créer, modifier et supprimer des stagiaires, des formateurs et des modules. Les stagiaires feront partis de groupes, et chaque module sera associé à un groupe de stagiaires et à un formateur. 

L'application est composé de 3 éléments :

- une base de données `mysql`
- un backend en java
- un frontend angular

![component diagram](conception/component-diagram.svg)



## Build

La construction de l'application peut être faite de la manière suivante.

### Backend

Avec `maven` et un interpreteur java en version >= 11, dans le sous-répertoire `formation-backend` :

``` sh
mvn clean package -DskipTests
```

L'artefact généré est situé ici : `formation-backend/target/formation-0.0.1.jar`.

### Frontend

Avec nodejs, dans le sous-répertoire `formation-frontend` :

```sh
npm run build
```

Le résultat du build est situé dans le répertoire `formation-frontend/dist`



## Tests unitaires et d'intégration

### Backend

Avec `maven` et un interpreteur java en version >= 11, les tests unitaires et d'intégration du backend peuvent être lancé par la commande suivante (en supposant que le projet a déjà été compilé) :

```sh
mvn surefire:test`
```

Les résultats des tests sont dans des fichiers XML dans le répertoire `formation-backend/target/surefire-reports/`.

### Frontend

```sh
npm run test-jenkins
```

Les résultats des tests sont dans des fichiers XML dans le répertoire `formation-frontend/reports/junit/reports.xml`.


