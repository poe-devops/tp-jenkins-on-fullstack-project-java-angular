package fr.formation.categories.controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.dto.FormateurDto;
import fr.formation.categories.models.Role;
import fr.formation.categories.services.FormateurService;

@RestController
@RequestMapping("/formateurs")
public class FormateurController extends GenericController<FormateurDto, FormateurService> {


	@GetMapping("search")
	public Collection<FormateurDto> search(@RequestParam() String q, Pageable pageable) {
		return service.search(q, pageable);
	}

	@Override
	public FormateurDto save(@Valid @RequestBody FormateurDto dto) {
		dto.setRole(Role.FORMATEUR);
		System.out.println(dto);
		return super.save(dto);
	}
	
}
