package fr.formation.categories.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.categories.dto.ParticipationDto;
import fr.formation.categories.services.ParticipationService;

@RestController
@RequestMapping("/participations")
public class ParticipationController extends GenericController<ParticipationDto, ParticipationService> {

}
