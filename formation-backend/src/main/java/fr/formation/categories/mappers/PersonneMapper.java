package fr.formation.categories.mappers;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;

import fr.formation.categories.dto.PersonneDto;
import fr.formation.categories.models.Personne;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface PersonneMapper extends fr.formation.categories.mappers.Mapper<Personne, PersonneDto> {

}
