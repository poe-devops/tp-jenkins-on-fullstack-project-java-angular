package fr.formation.categories.models;

import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Participation {
	
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected int id;

	@ManyToOne
	private Stagiaire stagiaire;
	
	@ManyToOne
	private Module module;
	
	@Embedded
	protected Evaluation evaluationModule;
	
	@Embedded
	protected Evaluation evaluationStagiaire;
	
	@OneToMany
	protected Set<Emargement> emargements;
	
}
