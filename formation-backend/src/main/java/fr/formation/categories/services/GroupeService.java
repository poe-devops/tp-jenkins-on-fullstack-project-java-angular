package fr.formation.categories.services;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.StagiaireDto;
import fr.formation.categories.mappers.GroupeMapper;
import fr.formation.categories.mappers.StagiaireMapper;
import fr.formation.categories.models.Groupe;
import fr.formation.categories.models.Stagiaire;
import fr.formation.categories.repositories.GroupeRepository;
import fr.formation.categories.repositories.StagiaireRepository;

@Service
public class GroupeService extends GenericService<GroupeMapper, GroupeRepository, Groupe, GroupeDto> {
	
	@Autowired
	private StagiaireRepository stagiaireRepository;
	
	@Autowired
	private StagiaireMapper stagiaireMapper;

	public Collection<GroupeDto> search(String q, Pageable pageable) {
		return repository.search(q, pageable).stream()
				.map(g -> mapper.modelToDto(g))
				.collect(Collectors.toList());
	}
	
	@Transactional
	public Collection<StagiaireDto> getStagiaires(int idGroupe) {
		return repository.findById(idGroupe).get().getStagiaires().stream()
				.map(s -> stagiaireMapper.modelToDto(s))
				.collect(Collectors.toList());
	}
	
	@Transactional
	public void setStagiaires(int groupeId, Collection<StagiaireDto> stagiaires) {
		Groupe g = repository.findById(groupeId).get();
		g.getStagiaires().clear();
		g.getStagiaires().addAll(stagiaires.stream()
				.map(dto -> stagiaireMapper.dtoToModel(dto))
				.collect(Collectors.toList()));
	}
	
	public void addStagiaire(int idStagiaire, int idGroupe) {
		// recuperer le groupe à partir de son id
		Groupe g = repository.findById(idGroupe).get();
		// recuperer le stagaiire  à partir de son id
		Stagiaire s = stagiaireRepository.findById(idStagiaire).get();
		// lier le stagaiire au groupe
		g.getStagiaires().add(s);
		s.getGroupes().add(g);
		// sauvegarder dans la base de données
		repository.save(g);
	}
	
	
}
