package fr.formation.categories.services;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.dto.ParticipationDto;
import fr.formation.categories.dto.StagiaireDto;
import fr.formation.categories.mappers.GroupeMapper;
import fr.formation.categories.mappers.ParticipationMapper;
import fr.formation.categories.mappers.StagiaireMapper;
import fr.formation.categories.models.Groupe;
import fr.formation.categories.models.Stagiaire;
import fr.formation.categories.repositories.GroupeRepository;
import fr.formation.categories.repositories.StagiaireRepository;

@Service
public class StagiaireService extends GenericService<StagiaireMapper, StagiaireRepository, Stagiaire, StagiaireDto> {
	
	@Autowired
	private GroupeRepository groupeRepository;
	
	@Autowired
	private GroupeMapper groupeMapper;
	
	@Autowired
	private ParticipationMapper participationMapper;

	@Override
	public void delete(Stagiaire m) {
		m.getGroupes().forEach(g -> {
			g.getStagiaires().removeIf(s -> s == m);
			groupeRepository.save(g);
		});
		super.delete(m);
	}

	public Collection<StagiaireDto> search(String q, Pageable pageable) {
		return repository.search(q, pageable).stream()
				.map(g -> mapper.modelToDto(g))
				.collect(Collectors.toList());
	}
	
	@Transactional
	public Collection<GroupeDto> getGroupes(int id) {
		return repository.findById(id).get().getGroupes().stream()
				.map(s -> groupeMapper.modelToDto(s))
				.collect(Collectors.toList());
	}
	
	@Transactional
	public void setGroupes(int id, Collection<GroupeDto> groupeDtos) {
		Stagiaire s = repository.findById(id).get();
		Collection<Groupe> groupes = groupeRepository.findAllById(groupeDtos.stream()
				.map(dto -> dto.getId())
				.collect(Collectors.toList()));
		// removing groupes
		s.getGroupes().stream()
			.filter(g -> !groupes.contains(g))
			.forEach(g -> {
				g.getStagiaires().remove(s);
				groupeRepository.save(g);
			});
		s.getGroupes().removeIf(g -> !groupes.contains(g));
		// adding groupes
		groupes.stream()
			.filter(g -> !s.getGroupes().contains(g))
			.forEach(g -> {
				s.getGroupes().add(g);
				g.getStagiaires().add(s);
				groupeRepository.save(g);
			});
	}
	
	@Transactional
	public Collection<ParticipationDto> getParticipations(int id) {
		return repository.findById(id).get().getParticipations().stream()
				.map(s -> participationMapper.modelToDto(s))
				.collect(Collectors.toList());
	}
	
}
