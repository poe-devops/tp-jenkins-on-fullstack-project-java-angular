import { AfterViewInit, Component, ElementRef, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTable } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, mergeMap, Observable, startWith, tap } from 'rxjs';
import { Groupe, groupeForm } from 'src/app/models/groupe';
import { Stagiaire } from 'src/app/models/stagiaires';
import { GroupeService } from 'src/app/services/groupe.service';
import { StagiaireService } from 'src/app/services/stagiaire.service';

@Component({
  selector: 'app-groupes',
  templateUrl: './groupes.component.html',
  styleUrls: ['./groupes.component.css']
})
export class GroupesComponent implements OnInit, AfterViewInit {

  groupes: Groupe[] = [];

  @ViewChild("groupesTableElement")
  groupesTable?: MatTable<any>;

  @ViewChild("drawer")
  drawer?: MatDrawer;

  selectedGroupe?: Groupe;

  sideTitle: string = "";
  groupeControl: FormGroup = groupeForm()

  stagiaireControl = new FormControl();

  filteredStagiaires: Observable<Stagiaire[]>;
  @ViewChild('stagiaireInput') stagiaireInput?: ElementRef<HTMLInputElement>;

  constructor(
    public ss: StagiaireService,
    public gs: GroupeService,
    public dialog: MatDialog,
    public router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.filteredStagiaires = this.stagiaireControl.valueChanges.pipe(
      startWith(""),
      filter(v => typeof v === "string"),
      mergeMap(q => ss.search(q, 10)),
      map(sl => sl.filter(s => !((this.groupeControl.get("stagiaires") as FormArray).value as Stagiaire[]).find(s1 => s.id == s1.id)))
    );
  }

  ngOnInit(): void {
    this.gs.findAll().subscribe(lg => this.groupes = lg);
    // this.gs.findAll().pipe(
    //   tap(lg => {
    //     this.groupes = lg;
    //   }),
    //   mergeMap(lg => this.activatedRoute.params.pipe(
    //     map(params => params['id']),
    //     map(id => lg.find(g => g.id == id)))
    // )).subscribe(g => {
    //   if (g)
    //     this.onSelect(g);
    // });
  }

  ngAfterViewInit(): void {
  }

  onAdd() {
    this.selectedGroupe = { id: 0, nom: "", nbStagiaires: 0 };
    this.sideTitle = "Nouveau groupe";
    this.stagiairesControl.clear();
    this.groupeControl.reset();
    this.groupeControl.setValue({ nom: "", stagiaires: [] });
    if (!this.drawer?.opened)
      this.drawer?.open();
  }

  onDelete() {
    if (this.selectedGroupe) {
      let g = this.selectedGroupe;
      this.gs.delete(g).subscribe({
        next: () => {
          this.groupes.splice(this.groupes.indexOf(g), 1);
          this.groupesTable?.renderRows();
          this.drawer?.close();
        }
      });
    }
  }

  onSelect(g: Groupe) {
    if (this.selectedGroupe == g && this.drawer?.opened) {
      this.drawer?.close();
    } else {
      this.selectedGroupe = g;
      this.sideTitle = "Groupe " + g.id;
      this.gs.getStagiaires(g).subscribe(ls => {
        this.groupeControl.reset();
        this.stagiairesControl.clear();
        this.groupeControl.setValue({ nom: g.nom, stagiaires: [] });
        ls.forEach(s => this.stagiairesControl.push(new FormControl(s)));
      });
      if (!this.drawer?.opened)
        this.drawer?.open();
    }
  }

  onClose() {
    // this.router.navigateByUrl("/groupes");
    this.selectedGroupe = undefined;
    this.drawer?.close();
  }

  onSubmit() {
    if (this.groupeControl.invalid) {
      this.groupeControl.markAllAsTouched();
      return;
    }
    if (this.selectedGroupe) {
      let g = this.selectedGroupe;
      let ls = this.groupeControl.get("stagiaires")?.value;
      g.nom = this.groupeControl.get("nom")?.value;
      g.nbStagiaires = ls.length;
      if (g.id != 0) {
        this.gs.update(this.selectedGroupe).subscribe({
          next: () => {
            this.gs.saveStagiaires(g, ls).subscribe({
              // next: () => this.editing = false
            });
          }
        });
      } else {
        this.gs.save(g as Groupe).subscribe({
          next: g => {
            this.gs.saveStagiaires(g, ls).subscribe({
              next: () => {
                this.drawer?.close();
                this.groupes.push(g);
                this.groupesTable?.renderRows();
              }
            });
          }
        });
      }
    }
  }

  addStagiaire(event: MatAutocompleteSelectedEvent) {
    (this.groupeControl.get("stagiaires") as FormArray).push(new FormControl(event.option.value as Stagiaire));
    // this.selectedStagiaires.push(event.option.value as Stagiaire);
    if (this.stagiaireInput)
      this.stagiaireInput.nativeElement.value = "";
    this.stagiaireControl.setValue("");
    this.stagiaireControl.updateValueAndValidity();
    this.groupeControl.markAsDirty();
  }

  removeStagiaire(index: number) {
    (this.groupeControl.get("stagiaires") as FormArray).removeAt(index);
    this.stagiaireControl.updateValueAndValidity();
    this.groupeControl.markAsDirty();
  }

  get stagiairesControl(): FormArray {
    return this.groupeControl.get("stagiaires") as FormArray;
  }


}
