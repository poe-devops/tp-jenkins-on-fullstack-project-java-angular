import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor';
import { AuthenticationService } from './services/authentication.service';
import { DisplayIfDirective } from './directives/display-if.directive';



@NgModule({
  declarations: [
    DisplayIfDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DisplayIfDirective
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
      deps: [AuthenticationService]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (as: AuthenticationService) => () => as.init(),
      deps: [AuthenticationService],
      multi: true
    }
  ],
})
export class SecurityModule { }
