-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: formations
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `groupe_personne`
--



DROP TABLE IF EXISTS `groupe_personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groupe_personne` (
  `groupes_id` int NOT NULL,
  `stagiaires_id` int NOT NULL,
  PRIMARY KEY (`groupes_id`,`stagiaires_id`),
  KEY `FK5otlgopavqryl05jpdp48po91` (`stagiaires_id`),
  CONSTRAINT `FK5otlgopavqryl05jpdp48po91` FOREIGN KEY (`stagiaires_id`) REFERENCES `personne` (`id`),
  CONSTRAINT `FKruyeivk9j69wak7cyiy5qi6am` FOREIGN KEY (`groupes_id`) REFERENCES `groupe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupe_personne`
--

LOCK TABLES `groupe_personne` WRITE;
/*!40000 ALTER TABLE `groupe_personne` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupe_personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participation`
--

DROP TABLE IF EXISTS `participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participation` (
  `id` int NOT NULL,
  `evaluation_module_commentaire` varchar(255) DEFAULT NULL,
  `evaluation_module_note` int NOT NULL,
  `evaluation_stagiaire_commentaire` varchar(255) DEFAULT NULL,
  `evaluation_stagiaire_note` int NOT NULL,
  `module_id` int DEFAULT NULL,
  `stagiaire_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9c3ukhs1847l1gvseo2n71d3e` (`module_id`),
  KEY `FKd1yrbofhax0xplcb6xao8896j` (`stagiaire_id`),
  CONSTRAINT `FK9c3ukhs1847l1gvseo2n71d3e` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `FKd1yrbofhax0xplcb6xao8896j` FOREIGN KEY (`stagiaire_id`) REFERENCES `personne` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participation`
--

LOCK TABLES `participation` WRITE;
/*!40000 ALTER TABLE `participation` DISABLE KEYS */;
/*!40000 ALTER TABLE `participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personne`
--

DROP TABLE IF EXISTS `personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personne` (
  `dtype` varchar(31) NOT NULL,
  `id` int NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `role` int DEFAULT NULL,
  `mot_de_passe` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personne`
--

LOCK TABLES `personne` WRITE;
/*!40000 ALTER TABLE `personne` DISABLE KEYS */;
/*!40000 ALTER TABLE `personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participation_emargement`
--

DROP TABLE IF EXISTS `participation_emargement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participation_emargement` (
  `participation_id` int NOT NULL,
  `emargements_id` int NOT NULL,
  PRIMARY KEY (`participation_id`,`emargements_id`),
  UNIQUE KEY `UK_pkwatw5q1y8ouo1onig228hjk` (`emargements_id`),
  CONSTRAINT `FKjyvxdeioib2939irbrehhruah` FOREIGN KEY (`participation_id`) REFERENCES `participation` (`id`),
  CONSTRAINT `FKn6r0yswdojy3o8je00353ydfp` FOREIGN KEY (`emargements_id`) REFERENCES `emargement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participation_emargement`
--

LOCK TABLES `participation_emargement` WRITE;
/*!40000 ALTER TABLE `participation_emargement` DISABLE KEYS */;
/*!40000 ALTER TABLE `participation_emargement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emargement`
--

DROP TABLE IF EXISTS `emargement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emargement` (
  `id` int NOT NULL,
  `date` date DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emargement`
--

LOCK TABLES `emargement` WRITE;
/*!40000 ALTER TABLE `emargement` DISABLE KEYS */;
/*!40000 ALTER TABLE `emargement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `module` (
  `id` int NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `jours` tinyblob,
  `nom` varchar(255) DEFAULT NULL,
  `formateur_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK317twhqufj47klxqw0xoyv0t4` (`formateur_id`),
  CONSTRAINT `FK317twhqufj47klxqw0xoyv0t4` FOREIGN KEY (`formateur_id`) REFERENCES `personne` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groupe` (
  `id` int NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupe`
--

LOCK TABLES `groupe` WRITE;
/*!40000 ALTER TABLE `groupe` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-01  9:13:25
