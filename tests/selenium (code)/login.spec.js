const { Builder, By, NoSuchElementError } = require('selenium-webdriver')
const assert = require('assert')


describe('Login', function() {

    this.timeout(300000)

    let driver

    beforeEach(async function() {
        // create driver
        driver = await new Builder()
            .usingServer('http://51.91.135.166:4444/wd/hub')
            .forBrowser('firefox')
            .build()
    })

    afterEach(async function() {
        // quit driver
        await driver.quit()
    })

    async function login(username, password) {
        // Open page
        await driver.get('https://practicetestautomation.com/practice-test-login/')
        // Type username student into Username field
        await driver.findElement(By.id('username')).sendKeys(username)
        // Type password Password123 into Password field
        await driver.findElement(By.id('password')).sendKeys(password)
        // Push Submit button
        await driver.findElement(By.id('submit')).click()
    }

    it('Test case 1: Positive LogIn test', async function() {
        // driver = await new Builder().forBrowser('firefox').build()
        // // Open page
        // await driver.get('https://practicetestautomation.com/practice-test-login/')
        // // Type username student into Username field
        // await driver.findElement(By.id('username')).sendKeys("student")
        // // Type password Password123 into Password field
        // await driver.findElement(By.id('password')).sendKeys("Password123")
        // // Push Submit button
        // await driver.findElement(By.id('submit')).click()
        await login("student", "Password123")
        // Verify new page URL contains practicetestautomation.com/logged-in-successfully/
        assert((await driver.getCurrentUrl()).includes('practicetestautomation.com/logged-in-successfully/'))
        // Verify new page contains expected text ('Congratulations' or 'successfully logged in')
        assert((await driver.findElement(By.css('.post-content strong')).getText()).toLowerCase().includes('congratulations'))
        // Verify button Log out is displayed on the new page
        assert(await driver.findElement(By.linkText('Log out')) != null)
    })



    it('Test case 2: Negative username test', async function() {
        // // Open page
        // await driver.get('https://practicetestautomation.com/practice-test-login/')
        // // Type username student into Username field
        // await driver.findElement(By.id('username')).sendKeys("incorrectUser")
        // // Type password Password123 into Password field
        // await driver.findElement(By.id('password')).sendKeys("Password123")
        // // Push Submit button
        // await driver.findElement(By.id('submit')).click()
        await login("incorrectUser", "Password123")
        // Verify error message is displayed
        assert(await driver.findElement(By.id('error')) != null)
        // Verify error message text is Your username is invalid!
        assert((await driver.findElement(By.id('error')).getText()).includes('username'))
    })

    it('Test case 3: Negative password test', async function() {
        // // Open page
        // await driver.get('https://practicetestautomation.com/practice-test-login/')
        // // Type username student into Username field
        // await driver.findElement(By.id('username')).sendKeys("student")
        // // Type password Password123 into Password field
        // await driver.findElement(By.id('password')).sendKeys("incorrectPassword")
        // // Push Submit button
        // await driver.findElement(By.id('submit')).click()
        await login("student", "incorrectPassword")
        // Verify error message is displayed
        assert(await driver.findElement(By.id('error')) != null)
        // Verify error message text is Your username is invalid!
        assert((await driver.findElement(By.id('error')).getText()).includes('password'))
    })

})